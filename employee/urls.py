"""HRMS URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, include,re_path
from employee import views
from django.utils.crypto import get_random_string


urlpatterns = [
    path('ajax/load-branches/', views.load_branches, name='ajax_load_branches'),
    path('user_type_employee_detail/', views.user_type_employee_detail, name='user_type_employee_detail'),

    # HOME

    # INSERT DEPARTMENT / SHOW DEPARTMENT
    path('department_dash/', views.department_dash, name="department_dash"),
    path('delete_department/<int:pid>', views.delete_department, name="delete_department"),
    path('updatedepartment/<str:department_name>', views.update_department, name="update_department"),
    path('department_related_employee/<int:pid>', views.department_related_employee, name="department_related_employee"),

    # INSERT DESIGNATION / SHOW DESIGNATION
    path('designation_dash/', views.designation_dash, name="designation_dash"),
    path('update_designation/<str:designation_name>', views.update_designation, name="update_designation"),
    path('delete_designation/<int:pid>', views.delete_designation, name="delete_designation"),

    # INSERT EMPLOYEE / SHOW EMPLOYEE
    path('employees_dash/', views.employees_dash, name="employees_dash"),
    path('delete_employee/<int:pid>', views.delete_employee, name="delete_employee"),
    path('update_employee/<int:pid>', views.update_employee, name="update_employee"),
    path('trainer_dash/', views.trainer_dash, name="trainer_dash"),
    path('update_intern/<int:pid>', views.update_intern, name="update_intern"),

    # SHOW_PARTICULAR_EMPLOYEE
    path('show_particular_employee/<int:pid>', views.show_particular_employee, name="show_particular_employee"),
    path('show_bank_details/', views.show_bank_details, name="show_bank_details"),
    path('add_bank_details/<str:Name>', views.add_bank_details, name="add_bank_details"),
    path('manage/', views.manage, name="manage"),
    path('export_employee/', views.export_employee, name="export_employee"),
    path('add_daily_work_reporting/', views.add_daily_work_reporting, name="add_daily_work_reporting"),
    path('daily_report_for_emp/', views.daily_report_for_emp, name="daily_report_for_emp"),
    path('add_daily_work_reporting_for_marketing/', views.add_daily_work_reporting_for_marketing, name="add_daily_work_reporting_for_marketing"),
    path('update_daily_work_reporting/<int:pid>', views.update_daily_work_reporting, name="update_daily_work_reporting"),
    path('update_daily_work_reporting_for_marketing/<int:pid>', views.update_daily_work_reporting_for_marketing, name="update_daily_work_reporting_for_marketing"),
    path('daily_work_reporting_to_hr/', views.daily_work_reporting_to_hr, name="daily_work_reporting_to_hr"),
    path('daily_work_reporting_to_hr_for_marketing/', views.daily_work_reporting_to_hr_for_marketing, name="daily_work_reporting_to_hr_for_marketing"),
    path('daily_work_reporting_to_head/', views.daily_work_reporting_to_head, name="daily_work_reporting_to_head"),
    path('date_filtered_daily_work_reporting_to_hr/<int:pid>', views.date_filtered_daily_work_reporting_to_hr, name="date_filtered_daily_work_reporting_to_hr"),
    path('manage_dates/', views.manage_dates, name="manage_dates"),
    path('date_filtered_daily_work_reporting_to_head/<int:pid>', views.date_filtered_daily_work_reporting_to_head, name="date_filtered_daily_work_reporting_to_head"),
    path('delete/', views.daily_work_view.as_view(), name="delete-work"),
    path('delete_date/<int:pid>', views.delete_date, name="delete_date"),
    path('monthly_report_for_head/', views.monthly_report_for_head, name="monthly_report_for_head"),
    path('salary_increment/', views.salary_increment, name="salary_increment"),
    path('increment_salary/<int:pid>', views.increment_salary, name="increment_salary"),
    path('employee_attendance/', views.employee_attendance, name="employee_attendance"),

]
