from django.contrib.auth.decorators import login_required

from django.shortcuts import render, redirect, get_object_or_404, HttpResponse, reverse
import random
from django.conf import settings
from .form import form_insert_employee, form_insert_department, form_insert_designation, form_add_bank_details, \
    form_edit_employee, form_edit_trainee, form_add_daily_work_reporting, form_add_date_daily_work, \
    increment_salary_form, form_add_daily_work_reporting_for_marketing, form_edit_daily_work_reporting_for_marketing
from .models import Employees, Department, Designation, Daily_work_reporting, Add_Date, \
    Daily_work_reporting_for_Marketing
from accounts.models import User, Notice_Period, Resignation, projects
from leaves.models import Leave_Type, Leave
from attendance.models import Attendace

from django.utils.timezone import timedelta
from django.views.generic import View
import schedule


# HOME
def index(request):
    return render(request, 'index.html')


def manage(request):
    form = Department.objects.all()
    data = Designation.objects.all()
    emp = Employees.objects.all()
    register = User.objects.all()
    leave_type = Leave_Type.objects.all()
    leave = Leave.objects.all()
    notice = Notice_Period.objects.all()
    resig = Resignation.objects.all()

    return render(request, 'manage.html',
                  {'form': form, 'data': data, 'emp': emp, 'register': register, 'leave_type': leave_type,
                   'leave': leave, 'notice': notice, 'resig': resig})


# .........................................DEPARTMENT..............................

# INSERT DEPARTMENT/ SHOW DEPARTMENT
def department_dash(request):
    data = Department.objects.all()
    if request.method == 'POST':
        form = form_insert_department(request.POST)
        if form.is_valid():
            form.save()
            return redirect('department_dash')
    else:
        form = form_insert_department()
    return render(request, 'department_dash.html', {'form': form, 'data': data})


def department_related_employee(request, pid):
    form = User.objects.filter(user_type="Department_Head")
    values = Department.objects.get(id=pid)
    return render(request, 'filtered_details.html', {'value': values, 'form': form})


def delete_department(request, pid):
    new = Department.objects.get(id=pid)
    new.delete()
    return redirect('manage')


# UPDATE DEPARTMENT
def update_department(request, department_name):
    new = Department.objects.get(id=department_name)
    if request.method == 'POST':
        form = form_insert_department(request.POST, instance=new)
        if form.is_valid():
            form.save()
            return redirect('department_dash')
    else:
        form = form_insert_department(instance=new)
    return render(request, 'edit.html', {'form': form})


# .........................................DESIGNATIONS..............................

# INSERT DESIGNATIONS/ SHOW DESIGNATIONS
def designation_dash(request):
    data = Designation.objects.all()
    if request.method == 'POST':
        form = form_insert_designation(request.POST)
        if form.is_valid():
            form.save()
            return redirect('designation_dash')
    else:
        form = form_insert_designation()
    return render(request, 'designation_dash.html', {'form': form, 'data': data})


def update_designation(request, designation_name):
    new = Designation.objects.get(id=designation_name)
    if request.method == 'POST':
        form = form_insert_designation(request.POST, instance=new)
        if form.is_valid():
            form.save()
            return redirect('designation_dash')
    else:
        form = form_insert_designation(instance=new)
    return render(request, 'edit.html', {'form': form})


def delete_designation(request, pid):
    new = Designation.objects.get(id=pid)
    new.delete()
    return redirect('manage')


# ........................................EMPLOYEES..............................

# INSERT EMPLOYEE/ SHOW EMPLOYEES
def employees_dash(request):
    data = Employees.objects.all()
    active = Employees.objects.filter(Emp_Status="Active")
    InActive = Employees.objects.filter(Emp_Status="InActive")
    block = Employees.objects.filter(Emp_Status="Block")
    if request.method == 'POST':
        form = form_insert_employee(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('employees_dash')
    else:
        form = form_insert_employee()
    return render(request, 'employees_dash.html',
                  {'form': form, 'data': data, 'active': active, 'block': block, 'InActive': InActive})


# EDIT EMPLOYEE
def update_employee(request, pid):
    new = Employees.objects.get(id=pid)
    if request.method == 'POST':
        form = form_edit_employee(request.POST, request.FILES, instance=new)
        if form.is_valid():
            form.save()
            return redirect('employees_dash')
    else:
        form = form_edit_employee(instance=new)
    return render(request, 'edit_employee.html', {'form': form})


def update_intern(request, pid):
    new = Employees.objects.get(id=pid)
    if request.method == 'POST':
        form = form_edit_trainee(request.POST, request.FILES, instance=new)
        if form.is_valid():
            form.save()
            return redirect('trainer_dash')
    else:
        form = form_edit_trainee(instance=new)
    return render(request, 'edit.html', {'form': form})


# DELETE EMPLOYEE
def delete_employee(request, pid):
    new = Employees.objects.get(id=pid)
    new.delete()
    return redirect('manage')


# SHOW PARTICULAR EMPLOYEE
def show_particular_employee(request, pid):
    form = Employees.objects.get(id=pid)
    return render(request, 'show_particular_employee.html', {'form': form})


def trainer_dash(request):
    data = Employees.objects.filter(Emp_Type="Intern")
    if request.method == 'POST':
        form = form_insert_employee(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('trainer_dash')
    else:
        form = form_insert_employee()
    return render(request, 'trainers.html', {'form': form, 'data': data})


# SHOW BANK DETAILS
def show_bank_details(request):
    form = Employees.objects.all()
    return render(request, 'bank_details.html', {'form': form})


# ADD BANK DETAILS
def add_bank_details(request, Name):
    new = Employees.objects.get(id=Name)
    if request.method == 'POST':
        form = form_add_bank_details(request.POST, instance=new)
        if form.is_valid():
            form.save()
            return redirect('show_bank_details')
    else:
        form = form_add_bank_details(instance=new)
    return render(request, 'edit.html', {'form': form})


def load_branches(request):
    depart_name_id = request.GET.get('Department')
    branches = Designation.objects.filter(depart_name_id=depart_name_id).order_by('designation_name')
    return render(request, 'designation_dropdown_list_options.html', {'branches': branches})


def user_type_employee_detail(request):
    b = Leave.objects.all()
    a = Employees.objects.all()
    return render(request, 'user_type_employee_detail.html', {'a': a, 'b': b})


def export_employee(request):
    all = Employees.objects.all()
    return render(request, 'export_employee.html', {'all': all})


def add_daily_work_reporting(request):
    daily_work = Daily_work_reporting.objects.filter(user=request.user.id)
    user = request.user
    if request.method == 'POST':
        form = form_add_daily_work_reporting(request.POST)
        if form.is_valid():
            data = form.save()
            data.user = user
            data.save()
            return redirect('add_daily_work_reporting')
    else:
        form = form_add_daily_work_reporting()
    return render(request, 'daily_work_reporting.html', {'form': form, 'daily_work': daily_work})


def add_daily_work_reporting_for_marketing(request):
    daily_work = Daily_work_reporting_for_Marketing.objects.filter(user_mkt=request.user.id)
    user = request.user
    if request.method == 'POST':
        form = form_add_daily_work_reporting_for_marketing(request.POST)
        if form.is_valid():
            data = form.save()
            data.user_mkt = user
            data.save()
            return redirect('add_daily_work_reporting_for_marketing')
    else:
        form = form_add_daily_work_reporting_for_marketing()
    return render(request, 'daily_work_reporting_for_marketing.html',
                  {'form': form, 'daily_work': daily_work})


def daily_report_for_emp(request):
    if request.method == "POST":
        fromdate = request.POST.get('fromdate')
        searchresult = Daily_work_reporting_for_Marketing.objects.raw('select id from '
                                                                      'employee_daily_work_reporting_for_marketing '
                                                                      'where date_mkt = "' + fromdate + '"')
        result_for_total = Daily_work_reporting_for_Marketing.objects.raw('select id,(total(interested)+total('
                                                                          'not_interested)+total('
                                                                          'ringing_SwitchOff_NR)) as totalcall,total('
                                                                          'new_boxes_order) as box_order from '
                                                                          'employee_daily_work_reporting_for_marketing '
                                                                          'where date_mkt = "' + fromdate + '" GROUP '
                                                                                                            'by user_mkt_id')
        return render(request, 'daily_report_for_emp.html',
                      {'data': searchresult, 'result_for_total': result_for_total})
    else:
        # display = Daily_work_reporting_for_Marketing.objects.all()
        return render(request, 'daily_report_for_emp.html')


def update_daily_work_reporting(request, pid):
    new = Daily_work_reporting.objects.get(id=pid)
    if request.method == 'POST':
        form = form_add_daily_work_reporting(request.POST, instance=new)
        if form.is_valid():
            form.save()
            return redirect('daily_work_reporting_to_hr')
    else:
        form = form_add_daily_work_reporting(instance=new)
    return render(request, 'edit.html', {'form': form})


def update_daily_work_reporting_for_marketing(request, pid):
    new = Daily_work_reporting_for_Marketing.objects.get(id=pid)
    if request.method == 'POST':
        form = form_edit_daily_work_reporting_for_marketing(request.POST, instance=new)
        if form.is_valid():
            form.save()
            return redirect('daily_work_reporting_to_head')
    else:
        form = form_edit_daily_work_reporting_for_marketing(instance=new)
    return render(request, 'edit.html', {'form': form})


def daily_work_reporting_to_hr(request):
    date = Add_Date.objects.all()
    daily_work = Daily_work_reporting.objects.all()

    return render(request, 'daily_work_reporting_to_hr.html',
                  {'daily_work': daily_work, 'date': date})


def daily_work_reporting_to_hr_for_marketing(request):
    if request.method == "POST":
        fromdate = request.POST.get('fromdate')
        todate = request.POST.get('todate')
        searchresult = Daily_work_reporting_for_Marketing.objects.raw('select id,total(new_boxes_order) as totalorder '
                                                                      'from '
                                                                      'employee_daily_work_reporting_for_marketing '
                                                                      'where date_mkt between "' + fromdate + '" '
                                                                                                              'and "' + todate + '" GROUP by user_mkt_id')
        return render(request, 'daily_work_reporting_to_hr_for_marketing.html',
                      {'data': searchresult, 'result_for_total': searchresult})
    else:
        # display = Daily_work_reporting_for_Marketing.objects.all()
        return render(request, 'daily_work_reporting_to_hr_for_marketing.html')


def manage_dates(request):
    date = Add_Date.objects.all()
    daily_work = Daily_work_reporting.objects.all()
    if request.method == 'POST':
        form = form_add_date_daily_work(request.POST)
        if form.is_valid():
            form.save()
            return redirect('manage_dates')
    else:
        form = form_add_date_daily_work()
    return render(request, 'manage_dates.html',
                  {'daily_work': daily_work, 'form': form, 'date': date})


def daily_work_reporting_to_head(request):
    date = Add_Date.objects.all()
    if request.method == "POST":
        fromdate = request.POST.get('fromdate')
        searchresult = Daily_work_reporting_for_Marketing.objects.raw('select id from '
                                                                      'employee_daily_work_reporting_for_marketing '
                                                                      'where date_mkt = "' + fromdate + '"')
        result_for_total = Daily_work_reporting_for_Marketing.objects.raw('select id,(total(interested)+total('
                                                                          'not_interested)+total('
                                                                          'ringing_SwitchOff_NR)) as totalcall,total('
                                                                          'new_boxes_order) as boxes_order from '
                                                                          'employee_daily_work_reporting_for_marketing '
                                                                          'where date_mkt = "' + fromdate + '" GROUP '
                                                                                                            'by user_mkt_id')
        return render(request, 'daily_work_reporting_to_head.html',
                      {'data': searchresult, 'result_for_total': result_for_total})
    else:
        # display = Daily_work_reporting_for_Marketing.objects.all()
        return render(request, 'daily_work_reporting_to_head.html', {'date': date})


def date_filtered_daily_work_reporting_to_hr(request, pid):
    new = Add_Date.objects.get(id=pid)
    dept = Daily_work_reporting.objects.all()
    return render(request, 'date_filtered_daily_work_reporting_to_hr.html', {'new': new, 'dept': dept})


def date_filtered_daily_work_reporting_to_head(request, pid):
    new = Add_Date.objects.get(id=pid)
    return render(request, 'date_filtered_daily_work_reporting_to_head.html', {'new': new})


def monthly_report_for_head(request):
    if request.method == "POST":
        fromdate = request.POST.get('fromdate')
        todate = request.POST.get('todate')
        searchresult = Daily_work_reporting_for_Marketing.objects.raw('select id,total(new_boxes_order) as totalorder '
                                                                      'from '
                                                                      'employee_daily_work_reporting_for_marketing '
                                                                      'where date_mkt between "' + fromdate + '" '
                                                                                                              'and "' + todate + '" GROUP by user_mkt_id')
        return render(request, 'monthly_report_for_head.html',
                      {'data': searchresult, 'result_for_total': searchresult})
    else:
        # display = Daily_work_reporting_for_Marketing.objects.all()
        return render(request, 'monthly_report_for_head.html')


# multiple delete
class daily_work_view(View):
    def get(self, request):
        new = Employees.objects.all()
        context = {
            'new': new
        }
        return render(request, 'multiple_delete.html', context)

    def post(self, request, *args, **kwargs):
        if request.method == "POST":
            pro_ids = request.POST.getlist('id[]')
            for id in pro_ids:
                update = Employees.objects.get(pk=id)
                update.delete()
            return redirect('delete-work')


def delete_date(request, pid):
    data = Add_Date.objects.get(id=pid)
    data.delete()
    return redirect('manage_dates')


def salary_increment(request):
    data = Employees.objects.all()
    dept = Department.objects.all()
    return render(request, 'salary_increment.html', {'data': data, 'dept': dept})


def increment_salary(request, pid):
    new = Employees.objects.get(id=pid)
    if request.method == 'POST':
        form = increment_salary_form(request.POST, instance=new)
        if form.is_valid():
            data = form.save()
            data.Salary = new.total_salary
            data.save()
            return redirect('salary_increment')
    else:
        form = increment_salary_form(instance=new)
    return render(request, 'edit.html', {'form': form})


def employee_attendance(request):
    user = request.user
    emp_data = Employees.objects.all()
    data = Employees.objects.filter(Name=user)
    get_all = Attendace.objects.filter(employee=user)
    return render(request, 'emp_attendance.html', {'get_all': get_all, 'data': data, 'emp_data': emp_data})

