from django.contrib import admin
from .models import Department, Employees, Designation, Daily_work_reporting, Add_Date,Daily_work_reporting_for_Marketing


# Register your models here.
class admin_designation(admin.ModelAdmin):
    list_display = ['depart_name', 'designation_name']
    list_filter = ['depart_name']


class admin_employee(admin.ModelAdmin):
    list_display = ['Name', 'Email']



admin.site.register(Department)
admin.site.register(Employees, admin_employee)
admin.site.register(Designation, admin_designation)
admin.site.register(Daily_work_reporting)
admin.site.register(Daily_work_reporting_for_Marketing)
admin.site.register(Add_Date)
