from django import forms
from .models import Employees, CHOICES_GENDER, Department, Designation, Daily_work_reporting, Add_Date, \
    Daily_work_reporting_for_Marketing


# from django.contrib.auth.models import User
class DateInput(forms.DateInput):
    input_type = 'date'
    format = '%d/%m/%Y'


class form_insert_department(forms.ModelForm):
    class Meta:
        model = Department
        fields = '__all__'


class form_insert_designation(forms.ModelForm):
    class Meta:
        model = Designation
        fields = '__all__'


class form_edit_employee(forms.ModelForm):
    class Meta:
        model = Employees
        fields = ('Name', 'Emp_ID', 'Employee_Image', 'Emp_Type', 'Emp_Status', 'Department', 'Designation', 'Number',
                  'Alternate_Number', 'Email',
                  'DOB',
                  'Gender', 'Adress', 'Blood_Grp',
                  'Training_date', 'Salary',
                  'Joining_date', 'Description_Box', 'Attach')


class form_edit_trainee(forms.ModelForm):
    class Meta:
        model = Employees
        fields = ('Name', 'Emp_Type', 'Number', 'Email',
                  'DOB', 'Adress', 'Blood_Grp',
                  'Training_date',
                  'Joining_date',)


class form_insert_employee(forms.ModelForm):
    class Meta:
        model = Employees
        fields = (
            'Name', 'Emp_ID', 'Employee_Image', 'Emp_Type', 'Emp_Status', 'Department', 'Designation', 'Number',
            'Alternate_Number', 'Email',
            'DOB',
            'Gender', 'Adress', 'Blood_Grp',
            'Training_date',
            'Joining_date', 'Salary', 'Description_Box', 'Attach')
        widgets = {
            'Training_date': DateInput(),
            'DOB': DateInput(),
            'Joining_date': DateInput(),
        }

    def clean_Email(self):
        Email = self.cleaned_data['Email']
        if Employees.objects.filter(Email=Email).exists():
            raise forms.ValidationError('Email already Exist')
        return Email

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['Designation'].queryset = Designation.objects.none()

        if 'Department' in self.data:
            try:
                depart_name_id = int(self.data.get('Department'))
                self.fields['Designation'].queryset = Designation.objects.filter(
                    depart_name_id=depart_name_id).order_by(
                    'designation_name')
            except (ValueError, TypeError):
                pass  # invalid input from the client; ignore and fallback to empty City queryset
        elif self.instance.pk:
            self.fields['Designation'].queryset = self.instance.Department.Designation_set.order_by('designation_name')


class form_add_bank_details(forms.ModelForm):
    class Meta:
        model = Employees
        fields = ('Bank_name', 'Bank_accnt_no', 'IFSC_Code', 'Branch', 'Card_Holder_Name')


class form_add_daily_work_reporting(forms.ModelForm):
    class Meta:
        model = Daily_work_reporting
        fields = ('project_name', 'project_work', 'date',)
        widgets = {
            'project_work': forms.Textarea(attrs={
                'rows': '1',
                'cols': '1',
                'maxlength': '50',
            }),
        }


class form_add_daily_work_reporting_for_marketing(forms.ModelForm):
    class Meta:
        model = Daily_work_reporting_for_Marketing
        fields = ('time', 'interested', 'not_interested', 'ringing_SwitchOff_NR', 'new_boxes_order',)


class form_edit_daily_work_reporting_for_marketing(forms.ModelForm):
    class Meta:
        model = Daily_work_reporting_for_Marketing
        fields = ('time', 'interested', 'not_interested', 'ringing_SwitchOff_NR', 'new_boxes_order', 'date_mkt',)
        widgets = {
            'date_mkt': DateInput
        }


class form_add_date_daily_work(forms.ModelForm):
    class Meta:
        model = Add_Date
        fields = ('add_date',)
        widgets = {
            'add_date': DateInput(),
        }


class increment_salary_form(forms.ModelForm):
    class Meta:
        model = Employees
        fields = ('Name', 'Increment', 'Increment_Month',)
        widgets = {
            'Increment_Month': DateInput(),
        }
