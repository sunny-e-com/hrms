# Generated by Django 3.1.5 on 2021-02-10 07:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('employee', '0007_auto_20210210_1249'),
    ]

    operations = [
        migrations.AlterField(
            model_name='daily_work_reporting',
            name='project_work',
            field=models.TextField(null=True),
        ),
    ]
