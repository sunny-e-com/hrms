# Generated by Django 3.1.5 on 2021-02-10 07:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('employee', '0005_auto_20210210_1224'),
    ]

    operations = [
        migrations.AlterField(
            model_name='daily_work_reporting_for_marketing',
            name='time',
            field=models.CharField(choices=[('10 to 11', '10 to 11'), ('11 to 12', '11 to 12'), ('12 to 1', '12 to 1'), ('2 to 3', '2 to 3'), ('3 to 4', '3 to 4'), ('4 to 5', '4 to 5'), ('5 to 6', '5 to 6')], max_length=30),
        ),
    ]
