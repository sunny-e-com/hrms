from django.db import models
from accounts.models import User
from datetime import datetime, timedelta, date

# Create your models here.

CHOICES_GENDER = (
    ('male', 'Male'),
    ('female', 'Female'),
    ('other', 'Other'),
)

CHOICES_BLOOD = (
    ('(A+)', '(A+)'),
    ('(A-)', '(A-)'),
    ('(B+)', '(B+)'),
    ('(B-)', '(B-)'),
    ('(O+)', '(O+)'),
    ('(O-)', 'O(O-)'),
    ('(AB+)', '(AB+)'),
    ('(AB-)', '(AB-)'),
    ('Other', 'Other'),
)

CHOICES_EMP_TYPE = (
    ('Intern', 'Intern'),
    ('Employee', 'Employee'),
)

CHOICES_EMP_STATUS = (
    ('Active', 'Active'),
    ('InActive', 'InActive'),
)

CHOICES_LEAVE_STATUS = (
    ('Pending', 'Pending'),
    ('Approved', 'Approved'),
    ('Rejected', 'Rejected'),
)

CHOICES_TIME_SLOT = (
    ('10 to 11', '10 to 11'),
    ('11 to 12', '11 to 12'),
    ('12 to 1', '12 to 1'),
    ('2 to 3', '2 to 3'),
    ('3 to 4', '3 to 4'),
    ('4 to 5', '4 to 5'),
    ('5 to 6', '5 to 6'),
)


class Department(models.Model):
    department_name = models.CharField(max_length=20)

    def __str__(self):
        return self.department_name


class Designation(models.Model):
    depart_name = models.ForeignKey(Department, on_delete=models.CASCADE, related_name="department_related_name")
    designation_name = models.CharField(max_length=20)

    def __str__(self):
        return self.designation_name


class Employees(models.Model):
    Name = models.CharField(max_length=30, blank=True, null=True)
    Emp_ID = models.CharField(max_length=20, blank=True, null=True)
    Employee_Image = models.ImageField(upload_to='files/', blank=True, null=True)
    Department = models.ForeignKey(Department, on_delete=models.CASCADE, related_name="connect_department")
    Designation = models.ForeignKey(Designation, on_delete=models.CASCADE, related_name="connect_designation")
    Number = models.PositiveIntegerField()
    Alternate_Number = models.PositiveIntegerField(blank=True, null=True)
    Email = models.EmailField(max_length=150, blank=True, null=True)
    DOB = models.DateField()
    Gender = models.CharField(max_length=20, choices=CHOICES_GENDER)
    Adress = models.TextField()
    Blood_Grp = models.CharField(max_length=20, choices=CHOICES_BLOOD)
    Emp_Type = models.CharField(max_length=20, choices=CHOICES_EMP_TYPE, default=CHOICES_EMP_TYPE[0])
    Training_date = models.DateField(blank=True, null=True)
    Joining_date = models.DateField(blank=True, null=True)
    Salary = models.PositiveIntegerField(blank=True, null=True, default=0)
    Increment = models.PositiveIntegerField(blank=True, null=True, default=0)
    Increment_Month = models.DateField(blank=True, null=True)
    Description_Box = models.TextField(blank=True, null=True)
    Emp_Status = models.CharField(max_length=30, choices=CHOICES_EMP_STATUS, default=CHOICES_EMP_TYPE[0])
    Attach = models.FileField(upload_to='files/', blank=True, null=True)
    Bank_name = models.CharField(max_length=25, blank=True, null=True)
    Bank_accnt_no = models.PositiveIntegerField(blank=True, null=True)
    IFSC_Code = models.CharField(max_length=20, blank=True, null=True)
    Branch = models.CharField(max_length=25, blank=True, null=True)
    Card_Holder_Name = models.CharField(max_length=25, blank=True, null=True)

    def __str__(self):
        return str(self.Name)

    @property
    def total_salary(self):
        return (self.Increment) + (self.Salary)


class Add_Date(models.Model):
    add_date = models.DateField()

    def __str__(self):
        return str(self.add_date)


class Daily_work_reporting(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user", blank=True, null=True)
    project_name = models.CharField(max_length=25, blank=True, null=True)
    project_work = models.TextField(null=True)
    date = models.ForeignKey(Add_Date, on_delete=models.CASCADE, related_name="date",  null=True)

    def __str__(self):
        return self.user.username


class Daily_work_reporting_for_Marketing(models.Model):
    user_mkt = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user_mkt", blank=True, null=True)
    time = models.CharField(max_length=30, choices=CHOICES_TIME_SLOT)
    interested = models.PositiveIntegerField(null=True,default=0)
    not_interested = models.PositiveIntegerField(null=True,default=0)
    ringing_SwitchOff_NR = models.PositiveIntegerField(verbose_name=u"Ringing/Switch off/NR",default=0)
    new_boxes_order = models.PositiveIntegerField(null=True,default=0)
    date_mkt = models.DateField(default=date.today)

    def __str__(self):
        return str(self.date_mkt)
