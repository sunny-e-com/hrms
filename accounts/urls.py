"""HRMS URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url
from django.urls import path, include
from accounts import views
from django.contrib.auth import views as djangoview

urlpatterns = [
    path('signup/', views.signup, name="signup"),
    path('logout/', djangoview.LogoutView.as_view(), name='logout'),
    path('register_detail/', views.register_detail, name="register_detail"),
    path('update_register/<str:username>', views.update_register_detail, name="update_register"),
    path('delete_register_detail/<int:pid>', views.delete_register_detail, name="delete_register_detail"),
    path('send/', views.sendanemail, name="email"),
    path('password-change/', djangoview.PasswordChangeView.as_view(template_name='account/change_password.html'),
         name='password_change'),
    path('password_change/done/',
         djangoview.PasswordChangeDoneView.as_view(template_name='account/password_changed_done.html'),
         name='password_change_done'),
    path('password-reset/',
         djangoview.PasswordResetView.as_view(template_name='account/password_reset.html',
                                              subject_template_name='account/password_reset_subject.txt',
                                              email_template_name='account/password_reset_email.html'),
         name='password_reset'),
    path('password-reset/done/',
         djangoview.PasswordResetDoneView.as_view(template_name='account/password_reset_done.html'),
         name='password_reset_done', ),
    path('password-reset-confirm/<uidb64>/<token>/',
         djangoview.PasswordResetConfirmView.as_view(template_name='account/password_reset_confirm.html'),
         name='password_reset_confirm'),
    path('password-reset-complete/',
         djangoview.PasswordResetCompleteView.as_view(template_name='account/password_reset_complete.html'),
         name='password_reset_complete'),
    path('techhub_invoice', views.techhub_invoice, name="techhub_invoice"),
    path('lblubricants_invoice/', views.lblubricants_invoice, name="lblubricants_invoice"),
    path('notice_period/', views.notice_period, name="notice_period"),
    path('update_notice_period/<int:pid>', views.update_notice_period, name="update_notice_period"),
    path('resignation/', views.resignation, name="resignation"),
    path('update_resignation/<int:pid>', views.update_resignation, name="update_resignation"),
    path('delete_notice_period/<int:pid>', views.delete_notice_period, name="delete_notice_period"),
    path('delete_resignation/<int:pid>', views.delete_resignation, name="delete_resignation"),
    path('add_projects/', views.add_projects, name="add_projects"),
    path('update_projects/<int:pid>', views.update_projects, name="update_projects"),
    path('delete_projects/<int:pid>', views.delete_projects, name="delete_projects"),
    path('project_details/<int:pid>', views.project_details, name="project_details"),

]
