from django.contrib import admin
from .models import User, Notice_Period


class admin_user(admin.ModelAdmin):
    list_display = ['username', 'email', 'user_type', 'is_superuser', ]


admin.site.register(User, admin_user)
admin.site.register(Notice_Period)


# Register your models here.
