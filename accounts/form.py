from django import forms
from django.contrib.auth.forms import UserCreationForm, PasswordResetForm
from django.core.exceptions import ValidationError
from .models import User, Notice_Period, Resignation, projects


class DateInput(forms.DateInput):
    input_type = 'date'
    format = '%d/%m/%Y'


class SignUpForm(UserCreationForm):
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2', 'user_type', 'dept',)

    def clean_email(self):
        email = self.cleaned_data['email']
        if User.objects.filter(email=email).exists():
            raise forms.ValidationError('Email already Exist')
        return email


class form_update_register(forms.ModelForm):
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'user_type', 'dept',)


class form_insert_notice_period(forms.ModelForm):
    class Meta:
        model = Notice_Period
        fields = (
            'Name', 'Date', 'Reason', 'attachment')
        widgets = {
            'Date': DateInput(),
        }


class form_insert_resignation(forms.ModelForm):
    class Meta:
        model = Resignation
        fields = (
            'Name', 'Date', 'attachment')
        widgets = {
            'Date': DateInput(),
        }


class form_insert_projects(forms.ModelForm):
    class Meta:
        model = projects
        fields = (
            'project_name', 'project_handling_by', 'date_of_project_taken', 'date_of_project_complition',
            'project_status', 'comments', 'attachment')
        widgets = {
            'date_of_project_taken': DateInput(),
            'date_of_project_complition': DateInput(),
        }
