from django.http import HttpResponse
from django.shortcuts import render, redirect
from .form import SignUpForm, form_update_register, form_insert_notice_period, form_insert_resignation, \
    form_insert_projects
from .models import User, Notice_Period, Resignation, projects
from django.conf import settings
from django.core.mail import send_mail
from django.contrib.auth import login


# Create your views here.
def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('index')
    else:
        form = SignUpForm()
    return render(request, 'account/signup.html', {'form': form})


def register_detail(request):
    data = User.objects.all()
    return render(request, 'registration.html', {'data': data})


def update_register_detail(request, username):
    new = User.objects.get(id=username)
    if request.method == 'POST':
        form = form_update_register(request.POST, instance=new)
        if form.is_valid():
            form.save()
            return redirect('register_detail')
    else:
        form = form_update_register(instance=new)
    return render(request, 'edit.html', {'form': form})


def delete_register_detail(request, pid):
    new = User.objects.get(id=pid)
    new.delete()
    return redirect('manage')


def sendanemail(request):
    if request.method == "POST":
        to = request.POST.get('toemail')
        content = request.POST.get('content')
        send_mail("testing", content, settings.EMAIL_HOST_USER, [to])
        return render(request, 'email.html', {'title': 'send an email'})
    else:
        return render(request, 'email.html', {'title': 'send an email'})


def techhub_invoice(request):
    return render(request, 'techhub_invoice.html')


def lblubricants_invoice(request):
    return render(request, 'lblubricants_invoice.html')


def notice_period(request):
    data = Notice_Period.objects.all()
    if request.method == 'POST':
        form = form_insert_notice_period(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('notice_period')
    else:
        form = form_insert_notice_period()
    return render(request, 'notice_period.html', {'form': form, 'data': data})


def update_notice_period(request, pid):
    new = Notice_Period.objects.get(id=pid)
    if request.method == 'POST':
        form = form_insert_notice_period(request.POST, request.FILES, instance=new)
        if form.is_valid():
            form.save()
            return redirect('notice_period')
    else:
        form = form_insert_notice_period(instance=new)
    return render(request, 'edit.html', {'form': form})


def delete_notice_period(request, pid):
    new = Notice_Period.objects.get(id=pid)
    new.delete()
    return redirect('manage')


def resignation(request):
    data = Resignation.objects.all()
    if request.method == 'POST':
        form = form_insert_resignation(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('resignation')
    else:
        form = form_insert_resignation()
    return render(request, 'resignation.html', {'form': form, 'data': data})


def update_resignation(request, pid):
    new = Resignation.objects.get(id=pid)
    if request.method == 'POST':
        form = form_insert_resignation(request.POST, request.FILES, instance=new)
        if form.is_valid():
            form.save()
            return redirect('resignation')
    else:
        form = form_insert_resignation(instance=new)
    return render(request, 'edit.html', {'form': form})


def delete_resignation(request, pid):
    new = Resignation.objects.get(id=pid)
    new.delete()
    return redirect('manage')


def add_projects(request):
    data = projects.objects.all()
    if request.method == 'POST':
        form = form_insert_projects(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('add_projects')
    else:
        form = form_insert_projects()
    return render(request, 'projects.html', {'form': form, 'data': data})


def update_projects(request, pid):
    new = projects.objects.get(id=pid)
    if request.method == 'POST':
        form = form_insert_projects(request.POST, request.FILES, instance=new)
        if form.is_valid():
            form.save()
            return redirect('add_projects')
    else:
        form = form_insert_projects(instance=new)
    return render(request, 'edit.html', {'form': form})


def delete_projects(request, pid):
    new = projects.objects.get(id=pid)
    new.delete()
    return redirect('add_projects')


def project_details(request, pid):
    data = projects.objects.get(id=pid)
    return render(request, 'project_details.html', {'data': data})
