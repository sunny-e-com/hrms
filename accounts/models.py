from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.
USER_TYPE_CHOICES = (
    ('user', 'User'),
    ('employee', 'Employee'),
    ('HR', 'HR'),
    ('Department_Head', 'Department_Head'),
)

CHOICES_PROJECT_STATUS = (
    ('pending', 'pending'),
    ('completed', 'completed'),
    ('returned', 'returned'),
)


class User(AbstractUser):
    user_type = models.CharField(max_length=20, choices=USER_TYPE_CHOICES, default=USER_TYPE_CHOICES[0])
    dept = models.ForeignKey("employee.Department", on_delete=models.CASCADE, related_name="dept", null=True,
                             blank=True)
    user_id = models.CharField(max_length=20, null=True)


class Notice_Period(models.Model):
    Name = models.ForeignKey("employee.Employees", on_delete=models.CASCADE, related_name="notice_des")
    Date = models.DateField()
    Reason = models.TextField()
    attachment = models.FileField(upload_to='files/', blank=True, null=True)

    def __str__(self):
        return self.Name


class Resignation(models.Model):
    Name = models.ForeignKey("employee.Employees", on_delete=models.CASCADE, related_name="resig_name")
    Date = models.DateField()
    attachment = models.FileField(upload_to='files/', blank=True, null=True)

    def __str__(self):
        return self.Name


class projects(models.Model):
    project_name = models.CharField(max_length=30)
    project_handling_by = models.ForeignKey("employee.Employees", on_delete=models.CASCADE,
                                            related_name="project_handler")
    date_of_project_taken = models.DateField()
    date_of_project_complition = models.DateField(null=True, blank=True)
    project_status = models.CharField(max_length=20, choices=CHOICES_PROJECT_STATUS, default=CHOICES_PROJECT_STATUS[0])
    comments = models.TextField(null=True, blank=True)
    attachment = models.FileField(upload_to='files/', blank=True, null=True)

    def __str__(self):
        return self.project_name
