$(document).ready(function(){
    var i=1;
    $("#add_row").click(function(){b=i-1;
      	$('#addr'+i).html($('#addr'+b).html()).find('td:first-child').html(i+1);
      	$('#tab_logic').append('<tr id="addr'+(i+1)+'"></tr>');
      	i++; 
  	});
    $("#delete_row").click(function(){
    	if(i>1){
		$("#addr"+(i-1)).html('');
		i--;
		}
		calc();
	});
	
	$('#tab_logic tbody').on('keyup change',function(){
		calc();
	});
	$('#tax').on('keyup change',function(){
		calc_total();
	});
	$('#cgst').on('keyup change',function(){
		calc_total1();
	});
	$('#sgst').on('keyup change',function(){
		calc_total1();
	});
	
	

});

function calc()
{
	$('#tab_logic tbody tr').each(function(i, element) {
		var html = $(this).html();
		if(html!='')
		{
			var qty = $(this).find('.qty').val();
			var price = $(this).find('.price').val();
			$(this).find('.total').val(qty*price);
			
			calc_total();
			calc_total1();
		}
    });
}

function calc_total()
{
	total=0;
	$('.total').each(function() {
        total += parseInt($(this).val());
    });
     dis_total=total-$('#discount').val();
    $('#sub_total').val(total.toFixed(2));
	tax_sum=dis_total/100*$('#tax').val();
	$('#tax_amount').val(tax_sum.toFixed(2));
	$('#total_amount').val((tax_sum+dis_total).toFixed(2));
	
	
}
function calc_total1(){
	total=0;
	$('.total').each(function() {
        total += parseInt($(this).val());
    });
    dis_total=total-$('#discount').val();
	$('#sub_total').val(total.toFixed(2));
	tax_sum=dis_total/100*$('#cgst').val();
	tax_sum1=dis_total/100*$('#sgst').val();
	ttx=tax_sum+tax_sum1;
	$('#tax_amount').val(ttx.toFixed(2));
	$('#total_amount').val((ttx+dis_total).toFixed(2));
}
function printpage() {

//Get the print button and put it into a variable
    var printButton = document.getElementById("printpagebutton");
    //Set the print button visibility to 'hidden'
    printButton.style.visibility = 'hidden';
    //Print the page content
    window.print()
    printButton.style.visibility = 'visible';
    }

//    $("input[type=date]").datepicker({
//      dateFormat: 'yy-mm-dd',
//      onSelect: function(dateText, inst) {
//        $(inst).val(dateText); // Write the value in the input
//  }
//});

// Code below to avoid the classic date-picker
$("input[type=date]").on('click', function() {
  return false;
});

