from django.contrib import admin
from .models import Attendace
from import_export.admin import ImportExportModelAdmin


class ViewAdmin(ImportExportModelAdmin):
    pass


class add_attendance(admin.ModelAdmin):
    list_display = ['date', 'employee', 'status']
    list_filter = ['employee']


admin.site.register(Attendace, add_attendance)
