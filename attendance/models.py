from time import strftime
from django.db import models

CHOICES_ATTENDANCE = (
    ('P', 'P'),
    ('A', 'A'),
    ('H', 'H'),

)


# Create your models here.

class Attendace(models.Model):
    date = models.DateField(null=True, blank=True)
    employee = models.CharField(max_length=15, null=True, blank=True)
    status = models.CharField(max_length=10, choices=CHOICES_ATTENDANCE, default=CHOICES_ATTENDANCE[0])
    reason = models.TextField(null=True, blank=True)
    atdn_emp_id = models.CharField(max_length=20, null=True)

    def __str__(self):
        return self.employee
