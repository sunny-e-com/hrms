"""HRMS URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, include
from attendance import views

urlpatterns = [
    path('upload-csv/', views.profile_upload, name="profile_upload"),
    path('attendance_by_name/', views.attendance_by_name, name="attendance_by_name"),
    path('atdn_name_filter/<int:pid>', views.atdn_name_filter, name="atdn_name_filter"),
    path('atd_filter_by_department_head/<int:pid>', views.atd_filter_by_department_head, name="atd_filter_by_department_head"),
    path('showresults/', views.showresults, name="attendance_showresults"),
    path('demo/', views.demo, name="demo"),
    path('demo1/', views.demo1, name="demo1"),
    path('update_attendance/', views.update_attendance, name="update_attendance"),
    ]
