import csv
import io
from datetime import date, timedelta
from django.contrib import messages
from django.shortcuts import render, redirect, HttpResponse
from tablib import Dataset

from employee.models import Employees, Daily_work_reporting
from django.http import JsonResponse
from .models import Attendace


def profile_upload(request):
    template = "upload_form.html"
    data = Attendace.objects.all()
    prompt = {
        'order': 'Order of the CSV should be date, employee, status,reason,atdn_emp_id',
        'profiles': data
    }
    if request.method == "GET":
        return render(request, template, prompt)
    csv_file = request.FILES['file']
    if not csv_file.name.endswith('.csv'):
        messages.error(request, 'THIS IS NOT A CSV FILE')
    data_set = csv_file.read().decode('UTF-8')
    io_string = io.StringIO(data_set)
    next(io_string)
    for column in csv.reader(io_string, delimiter=',', quotechar="|"):
        _, created = Attendace.objects.update_or_create(
            date=column[0],
            employee=column[1],
            status=column[2],
            reason=column[3],
            atdn_emp_id=column[4],
        )
    context = {}
    return render(request, template, context)


def attendance_by_name(request):
    try:
        if request.method == "POST":
            fromdate = request.POST.get('fromdate')
            todate = request.POST.get('todate')
            delete_atdn = Attendace.objects.raw(
                'DELETE FROM attendance_attendace WHERE date BETWEEN "' + fromdate + '" '
                                                                                     'and "' + todate + '"')
            return render(request, 'attendance_by_name.html',
                          {'delete_atdn': delete_atdn})
        else:
            name = Employees.objects.all()
            display = Attendace.objects.raw('SELECT id,employee FROM attendance_attendace GROUP by employee')
            return render(request, 'attendance_by_name.html', {'name': name, 'emp': display})
    except TypeError:
        return redirect('attendance_by_name')


def atdn_name_filter(request, pid):
    emp_emp = Employees.objects.get(id=pid)
    get_all = Attendace.objects.all()
    if request.method == "POST":
        emp_emp = Employees.objects.get(id=pid)
        fromdate = request.POST.get('fromdate')
        todate = request.POST.get('todate')
        searchresult = Attendace.objects.raw(
            'select * from attendance_attendace where  date between "' + fromdate + '" '
                                                                                    'and "' + todate + '"')
        return render(request, 'atdn_name_filter.html', {'emp_emp': emp_emp, 'data': searchresult})
    else:
        display = Attendace.objects.all()
    return render(request, 'atdn_name_filter.html', {'emp_emp': emp_emp, 'get_all': get_all, 'data': display})


def showresults(request):
    if request.method == "POST":
        fromdate = request.POST.get('fromdate')
        todate = request.POST.get('todate')
        searchresult = Attendace.objects.raw('select id, count(case status when "A" then 1 else null end) as A,'
                                             'count(case status when "P" then 1 else null end) as P,count(case status '
                                             'when "H" then 1 else null end) as H, count(case status when "H" then 1 '
                                             'else null end)*0.5 +count(case status when "A" then 1 else null end) as '
                                             'totalabsent,(count(case status when "P" then 1 else null end)*100)/('
                                             'count(case status when "A" then 1 else null end)+ count(case status '
                                             'when "P" then 1 else null end)+count(case status when "H" then 1 else '
                                             'null end)) as total_present_percentage,(100-(count(case status when "P" '
                                             'then 1 else null end)*100)/(count(case status when "A" then 1 else null '
                                             'end)+ count(case status when "P" then 1 else null end)+count(case '
                                             'status when "H" then 1 else null end))) as total_absent_percentage from '
                                             'attendance_attendace where '
                                             'date between "' + fromdate + '" and "' + todate + '" GROUP by employee')
        return render(request, 'attendance_filter.html',
                      {'data': searchresult})
    else:
        display = Attendace.objects.all()
        return render(request, 'attendance_filter.html')


def atd_filter_by_department_head(request, pid):
    emp_emp = Employees.objects.get(id=pid)
    get_all = Attendace.objects.all()
    if request.method == "POST":
        emp_emp = Employees.objects.get(id=pid)
        fromdate = request.POST.get('fromdate')
        todate = request.POST.get('todate')
        searchresult = Attendace.objects.raw(
            'select * from attendance_attendace where  date between "' + fromdate + '" '
                                                                                    'and "' + todate + '"')
        return render(request, 'atd_filter_by_department_head.html', {'emp_emp': emp_emp, 'data': searchresult})
    else:
        display = Attendace.objects.all()
    return render(request, 'atd_filter_by_department_head.html',
                  {'emp_emp': emp_emp, 'get_all': get_all, 'data': display})


def update_attendance(request):
    try:
        if request.method == "POST":
            status = request.POST.get('status')
            employee = request.POST.get('employee')
            fromdate = request.POST.get('fromdate')
            todate = request.POST.get('todate')
            searchresult = Attendace.objects.raw(
                'UPDATE attendance_attendace SET status ="' + status + '" WHERE employee = '
                                                                       '"' + employee + '" '
                                                                                        ' AND date between "' + fromdate + '" and "' + todate + '"')
            return render(request, 'update_attendance.html',
                          {'data': searchresult})
        else:
            display = Attendace.objects.raw('SELECT id,employee FROM attendance_attendace GROUP by employee')
            return render(request, 'update_attendance.html', {'emp': display})
    except TypeError:
        display = Attendace.objects.raw('SELECT id,employee FROM attendance_attendace GROUP by employee')
        return render(request, 'update_attendance.html', {'emp': display})


# schedule.every(50).seconds.do(lets_try)
# while 1:
#     schedule.run_pending()
#     time.sleep(1)

# UPDATE attendance_attendace SET status="A" WHERE date BETWEEN(SELECT leave_starting_date FROM leaves_leave WHERE emp_id=atdn_emp_id) AND (SELECT leave_ending_date FROM leaves_leave WHERE emp_id=atdn_emp_id)
def demo(request):
    template = "demo.html"
    data = Attendace.objects.all()
    prompt = {
        'order': 'Order of the CSV should be date, employee, status,reason,atdn_emp_id',
        'profiles': data
    }
    if request.method == "GET":
        return render(request, template, prompt)
    csv_file = request.FILES['file']
    if not csv_file.name.endswith('.csv'):
        messages.error(request, 'THIS IS NOT A CSV FILE')
    data_set = csv_file.read().decode('UTF-8')
    io_string = io.StringIO(data_set)
    next(io_string)
    for column in csv.reader(io_string, delimiter=',', quotechar="|"):
        _, created = Attendace.objects.update_or_create(
            date=column[0],
            employee=column[1],
            status=column[2],
            reason=column[3],
            atdn_emp_id=column[4],
        )
    context = {}
    return render(request, template, context)


def demo1(request):
    return render(request, 'demo1.html')
