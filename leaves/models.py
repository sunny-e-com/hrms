from django.db import models
from accounts.models import User
from datetime import datetime
from employee.models import Department
from datetime import datetime, timedelta

# Create your models here.
CHOICES_LEAVE_STATUS = (
    ('Pending', 'Pending'),
    ('Approved', 'Approved'),
    ('Rejected', 'Rejected'),
)
CHOICES_half_full_day = (
    ('HalfDay', 'HalfDay'),
    ('FullDay', 'FullDay'),
)


class Leave_Type(models.Model):
    leave_type_name = models.CharField(max_length=35)

    def __str__(self):
        return self.leave_type_name


class Leave(models.Model):
    leave_type = models.ForeignKey(Leave_Type, on_delete=models.PROTECT, related_name="type_relate")
    leave_starting_date = models.DateField()
    leave_ending_date = models.DateField()
    reason = models.TextField()
    leave_status = models.CharField(max_length=30, choices=CHOICES_LEAVE_STATUS, default="Pending")
    HR_leave_status = models.CharField(max_length=30, choices=CHOICES_LEAVE_STATUS, default="Pending")
    Department_head_leave_status = models.CharField(max_length=30, choices=CHOICES_LEAVE_STATUS, default="Pending")
    leave_document = models.FileField(upload_to='files/', blank=True, null=True)
    user_get = models.ForeignKey(User, related_name="user_get", on_delete=models.CASCADE, blank=True, null=True)
    additional_comment = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(default=datetime.now)
    half_full_day = models.CharField(max_length=30, choices=CHOICES_half_full_day, default="FullDay")
    emp_id = models.CharField(max_length=20,null=True)

    @property
    def subtract(self):
        return self.leave_ending_date - self.leave_starting_date

    @property
    def datefield(self):
        return self.leave_ending_date - timedelta(days=12)
        # return self.leave_ending_date - self.leave_starting_date
