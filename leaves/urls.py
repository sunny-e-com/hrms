"""HRMS URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import url
from leaves import views
urlpatterns = [
    path('leave_dash/', views.leave_dash, name="leave_dash"),
    path('update_leave_dash/<int:pid>', views.update_leave_dash, name="update_leave_dash"),
    path('leave_type/', views.leave_type, name="leave_type"),
    path('add_leave_type/', views.add_leave_type, name="add_leave_type"),
    path('department_head_leave_view/', views.department_head_leave_view, name="department_head_leave_view"),
    path('delete_leave_type/<int:pid>', views.delete_leave_type, name="delete_leave_type"),
    path('update_leave_type/<int:pid>', views.update_leave_type, name="update_leave_type"),
    path('update_leave_admin_dash/<int:pid>', views.update_leave_admin_dash, name="update_leave_admin_dash"),
    path('update_leave_HR_Staus/<int:pid>', views.update_leave_HR_Staus, name="update_leave_HR_Staus"),
    path('update_leave_department_head_status/<int:pid>', views.update_leave_department_head_status, name="update_leave_department_head_status"),
    path('delete_leave/<int:pid>', views.delete_leave, name="delete_leave"),
    path('delete_leave_from_employee/<int:pid>', views.delete_leave_from_employee, name="delete_leave_from_employee"),
    path('leave_status_approved/', views.leave_status_approved, name="leave_status_approved"),
    path('leave_status_pending/', views.leave_status_pending, name="leave_status_pending"),
    path('leave_status_rejected/', views.leave_status_rejected, name="leave_status_rejected"),
    path('showresults/', views.showresults, name="showresults"),
    path('print_leave/<int:pid>', views.print_leave, name="print_leave"),
    path('show_employee_leave_status/<str:leave_status>', views.show_employee_leave_status, name="show_employee_leave_status"),
    path('try_one/', views.try_one, name="try_one"),
]
