from django.contrib import admin
from .models import Leave_Type, Leave

class leave_type(admin.ModelAdmin):
    list_display = ['user_get','leave_type','leave_starting_date',]
    list_filter = ['leave_starting_date']

# Register your models here.
admin.site.register(Leave_Type)
admin.site.register(Leave,leave_type)
