from django import forms
from django.forms import ClearableFileInput
from .models import Leave, Leave_Type
from django.db import models


class DateInput(forms.DateInput):
    input_type = 'date'


class form_insert_leave(forms.ModelForm):
    class Meta:
        model = Leave
        fields = ('leave_type', 'leave_starting_date', 'leave_ending_date', 'reason', 'leave_document',)
        widgets = {
            'leave_starting_date': DateInput(),
            'leave_ending_date': DateInput(),
        }


class form_edit_leave_admin_dash(forms.ModelForm):
    class Meta:
        model = Leave
        fields = ('leave_status', 'additional_comment')


class form_edit_leave_HR_Status(forms.ModelForm):
    class Meta:
        model = Leave
        fields = ('HR_leave_status', 'half_full_day', 'additional_comment')


class form_edit_leave_Department_head_Status(forms.ModelForm):
    class Meta:
        model = Leave
        fields = ('Department_head_leave_status', 'additional_comment')


class form_add_leave_type(forms.ModelForm):
    class Meta:
        model = Leave_Type
        fields = '__all__'
