from django.contrib.auth.decorators import login_required
from django.db.models import Count
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render, redirect, get_object_or_404, HttpResponse, reverse
from django.conf import settings

from attendance.models import Attendace
from .form import form_insert_leave, form_add_leave_type, form_edit_leave_admin_dash, form_edit_leave_HR_Status, \
    form_edit_leave_Department_head_Status
from .models import Leave, Leave_Type
from accounts.models import User
from employee.models import Employees, Department


# Create your views here.

def leave_dash(request):
    data = Leave.objects.filter(user_get=request.user.id)
    user = request.user
    user_id = request.user.user_id
    if request.method == 'POST':
        form = form_insert_leave(request.POST, request.FILES)
        if form.is_valid():
            data = form.save()
            data.user_get = user
            data.emp_id = user_id
            data.save()
            return redirect('leave_dash')
    else:
        form = form_insert_leave()
    return render(request, 'leave.html', {'form': form, 'data': data})


def update_leave_dash(request, pid):
    new = Leave.objects.get(id=pid)
    if request.method == 'POST':
        form = form_insert_leave(request.POST, request.FILES, instance=new)
        if form.is_valid():
            form.save()
            return redirect('leave_dash')
    else:
        form = form_insert_leave(instance=new)
    return render(request, 'edit.html', {'form': form})


def delete_leave_from_employee(request, pid):
    new = Leave.objects.get(id=pid)
    new.delete()
    return redirect('leave_dash')


def delete_leave(request, pid):
    new = Leave.objects.get(id=pid)
    new.delete()
    return redirect('manage')


def update_leave_admin_dash(request, pid):
    new = Leave.objects.get(id=pid)
    if request.method == "POST":
        form = form_edit_leave_admin_dash(request.POST, request.FILES, instance=new)
        if form.is_valid():
            form.save()
            return redirect('leave_type')
    else:
        form = form_edit_leave_admin_dash(instance=new)
    return render(request, 'edit.html', {'form': form})


def update_leave_department_head_status(request, pid):
    new = Leave.objects.get(id=pid)
    if request.method == "POST":
        form = form_edit_leave_Department_head_Status(request.POST, request.FILES, instance=new)
        if form.is_valid():
            form.save()
            return redirect('department_head_leave_view')
    else:
        form = form_edit_leave_Department_head_Status(instance=new)
    return render(request, 'edit.html', {'form': form})


def update_leave_HR_Staus(request, pid):
    new = Leave.objects.get(id=pid)
    if request.method == "POST":
        form = form_edit_leave_HR_Status(request.POST, request.FILES, instance=new)
        if form.is_valid():
            form.save()
            return redirect('leave_type')
    else:
        form = form_edit_leave_HR_Status(instance=new)
    return render(request, 'edit.html', {'form': form})


def leave_type(request):
    try:
        b = Employees.objects.all()
        data = Leave.objects.all()
        cont = Leave.objects.count()
        count1 = Leave.objects.filter(leave_status="Pending")
        count2 = Leave.objects.filter(leave_status="Approved")
        count3 = Leave.objects.filter(leave_status="Rejected")
        Pending = count1.count()
        Approved = count2.count()
        Rejected = count3.count()
        if request.method == "POST":
            searchresult = Attendace.objects.raw(
                'UPDATE attendance_attendace SET status="A" WHERE date BETWEEN(SELECT leave_starting_date FROM '
                'leaves_leave WHERE emp_id=atdn_emp_id AND leave_status="Approved") AND (SELECT leave_ending_date '
                'FROM leaves_leave WHERE emp_id=atdn_emp_id AND leave_status="Approved")')
            return render(request, 'admin_leave.html',
                          {'data': searchresult})
        else:
            return render(request, 'admin_leave.html',
                          {'data': data, 'All': cont, 'Pending': Pending, 'Approved': Approved,
                           'Rejected': Rejected, 'b': b})
    except TypeError:
        return redirect('leave_type')


def department_head_leave_view(request):
    data = Leave.objects.all()
    b = Employees.objects.all()
    return render(request, 'department_head_leave_view.html', {'data': data, 'b': b})


def add_leave_type(request):
    data = Leave_Type.objects.all()
    if request.method == 'POST':
        form = form_add_leave_type(request.POST)
        if form.is_valid():
            form.save()
            return redirect('add_leave_type')
    else:
        form = form_add_leave_type()
    return render(request, 'add_leave_type.html', {'form': form, 'data': data})


def update_leave_type(request, pid):
    new = Leave_Type.objects.get(id=pid)
    if request.method == 'POST':
        form = form_add_leave_type(request.POST, instance=new)
        if form.is_valid():
            form.save()
            return redirect('manage')
    else:
        form = form_add_leave_type(instance=new)
    return render(request, 'edit.html', {'form': form})


def delete_leave_type(request, pid):
    new = Leave_Type.objects.get(id=pid)
    new.delete()
    return redirect('manage')


def leave_status_approved(request):
    new = Leave.objects.filter(leave_status="Approved")
    cont = Leave.objects.count()
    count1 = Leave.objects.filter(leave_status="Pending")
    count3 = Leave.objects.filter(leave_status="Rejected")
    Pending = count1.count()
    Approved = new.count()
    Rejected = count3.count()
    return render(request, 'admin_leave.html',
                  {'data': new, 'All': cont, 'Pending': Pending, 'Approved': Approved, 'Rejected': Rejected})


def leave_status_pending(request):
    new = Leave.objects.filter(leave_status="Pending")
    cont = Leave.objects.count()
    count1 = Leave.objects.filter(leave_status="Approved")
    count3 = Leave.objects.filter(leave_status="Rejected")
    Pending = new.count()
    Approved = count1.count()
    Rejected = count3.count()
    return render(request, 'admin_leave.html',
                  {'data': new, 'All': cont, 'Pending': Pending, 'Approved': Approved, 'Rejected': Rejected})


def leave_status_rejected(request):
    cont = Leave.objects.count()
    new = Leave.objects.filter(leave_status="Rejected")
    count1 = Leave.objects.filter(leave_status="Pending")
    count3 = Leave.objects.filter(leave_status="Approved")
    Pending = count1.count()
    Approved = count3.count()
    Rejected = new.count()
    return render(request, 'admin_leave.html',
                  {'data': new, 'All': cont, 'Pending': Pending, 'Approved': Approved, 'Rejected': Rejected})


def showresults(request):
    if request.method == "POST":
        fromdate = request.POST.get('fromdate')
        todate = request.POST.get('todate')
        searchresult = Leave.objects.raw(
            'select * from leaves_leave where leave_starting_date between "' + fromdate + '" '
                                                                                          'and "' + todate + '"')
        return render(request, 'admin_leave_filter.html', {'data': searchresult})
    else:
        display = Leave.objects.all()
        return render(request, 'admin_leave_filter.html', {'data': display})


def show_employee_leave_status(request, leave_status):
    data = Leave.objects.get(id=leave_status)
    return render(request, 'show_employee_leave_status.html', {'data': data})


def print_leave(request, pid):
    a = Leave.objects.get(id=pid)
    new = Employees.objects.all()
    return render(request, 'leaveform.html', {'leave': a, 'emp': new})


def try_one(request):
    data = Leave.objects.all()
    new = Employees.objects.all()
    return render(request, 'try.html', {'leave': data, 'emp': new})
