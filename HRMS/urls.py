"""HRMS URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls import url
from django.views.static import serve
from employee import views
from django.contrib.auth import views as djangoview
urlpatterns = [
    path('', djangoview.LoginView.as_view(template_name='account/signin.html'), name='login'),
    path('index/', views.index, name="index"),
    path('accounts/', include('accounts.urls')),
    path('employee/', include('employee.urls')),
    path('leaves/', include('leaves.urls')),
    path('attendance/', include('attendance.urls')),
    path('my_personal_use_for_admin/', admin.site.urls),

]

urlpatterns += [url(r'^media/(?P<path>.*)$', serve, {
    'document_root': settings.MEDIA_ROOT,
}),
                ]
